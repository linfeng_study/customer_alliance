const menulist = [{
		"name": "衣帽服饰",
		"iconUrl": "../../static/shopimg/icon-menu-yimao.png",
		"page": ""
	},
	{
		"name": "护肤美妆",
		"iconUrl": "../../static/shopimg/icon-menu-hufu.png",
		"page": ""
	},
	{
		"name": "母婴专区",
		"iconUrl": "../../static/shopimg/icon-menu-muying.png",
		"page": ""
	},
	{
		"name": "家具用品",
		"iconUrl": "../../static/shopimg/icon-menu-jiaju.png",
		"page": ""
	},
	{
		"name": "数码家电",
		"iconUrl": "../../static/shopimg/icon-menu-shuma.png",
		"page": ""
	},
	{
		"name": "充值中心",
		"iconUrl": "../../static/shopimg/icon-menu-chongzhi.png",
		"page": ""
	},
	{
		"name": "创业币专区",
		"iconUrl": "../../static/shopimg/icon-menu-chongye.png",
		"page": ""
	},
	{
		"name": "团购专区",
		"iconUrl": "../../static/shopimg/icon-menu-tuangou.png",
		"page": ""
	},
	{
		"name": "限时秒杀",
		"iconUrl": "../../static/shopimg/icon-menu-xianshi.png",
		"page": ""
	},
	{
		"name": "全部分类",
		"iconUrl": "../../static/shopimg/icon-menu-fenlei.png",
		"page": ""
	}
]

const tabBarList = [
	{
		"index":0,
		"name":"返回",
		"page":"../fast/fast",
		"iconPath":"",
		"selectedIconPath":""
	},
	{
		"index":1,
		"name":"首页",
		"page":"../shop/index",
		"iconPath":"../../static/shopimg/icon-bar-index02.png",
		"selectedIconPath":"../../static/shopimg/icon-bar-index01.png"
	},
	{
		"index":2,
		"name":"消息",
		"page":"",
		"iconPath":"../../static/shopimg/icon-bar-msg02.png",
		"selectedIconPath":"../../static/shopimg/icon-bar-msg01.png"
	},
	{
		"index":3,
		"name":"购物车",
		"page":"",
		"iconPath":"../../static/shopimg/icon-bar-shopcar02.png",
		"selectedIconPath":"../../static/shopimg/icon-bar-shopcar01.png"
	},
	{
		"index":4,
		"name":"我的",
		"page":"",
		"iconPath":"../../static/shopimg/icon-bar-wode02.png",
		"selectedIconPath":"../../static/shopimg/icon-bar-wode01.png"
	}
]

module.exports = {
	menulist: menulist,
	tabBarList:tabBarList
}

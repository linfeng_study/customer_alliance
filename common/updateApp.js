function plusReady(url) {
	// 获取本地应用资源版本号  
	plus.runtime.getProperty(plus.runtime.appid, function(inf) {
		// var wgtVer = inf.version; //获取当前版本号
		// version = plus.runtime.version; //获取当前版本号 简写
		uni.getSystemInfo({
			success: (res) => {
				if (res.platform == "android") { // 检查是否安卓
					updateApp(url, inf.version)
				} else {
					updateAppIos(url, inf.version);
				}
			}
		})
	})
};

function updateApp(url, version) {
	var title = "";
	uni.request({
		url: url + '/real/appVerion', //获取最新版本号
		method: 'POST',
		data: {},
		success: res => {
			if (res.data.data.version != version) {
				if (plus.networkinfo.getCurrentType() != 3) {
					title = "有新的版本发布，检测到您目前是手机流量连接，是否继续下载？";
				} else {
					title = "有新的版本发布，检测到您目前是Wifi连接，是否继续下载？";
				}
				uni.showModal({
					title: '提示',
					content: title,
					icon: "none",
					success: function(res) {
						if (res.confirm) {
							gengxin(url, version);
							uni.showToast({
								title: '软件已在后台下载中。新版本下载完成后将自动弹出安装程序。',
								mask: false,
								duration: 5000,
								icon: "none"
							});
						} else if (res.cancel) {
							return;
						}
					},
				});
			} else {
				uni.showToast({
					title: '已经是最新版本了',
					mask: false,
					duration: 5000,
					icon: "none"
				});
			}

		},
		fail: () => {},
		complete: () => {}
	});
}

function gengxin(url, version) {
	uni.request({
		url: url + '/real/appVerion', //获取最新版本号
		method: 'POST',
		data: {},
		success: res => {
			//下载资源包
			var dtask = plus.downloader.createDownload("http://52sg.vip/source/pack/upload/install/install.php?id=4445", {}, function(d, status) {
				// 下载完成  
				if (status == 200) {
					plus.nativeUI.closeWaiting();
					plus.runtime.install(
						plus.io.convertLocalFileSystemURL(d.filename), {}, {},
						function(error) {
							uni.showToast({
								title: '安装失败',
								mask: false,
								duration: 1500
							});
						})
				} else {
					uni.showToast({
						title: '更新失败',
						mask: false,
						duration: 1500
					});
				}
			});
			setTimeout(() => {
				var wgtWaiting = plus.nativeUI.showWaiting("开始下载");
				dtask.addEventListener("statechanged", function(d, status) {
					switch (d.state) {
						case 2:
							wgtWaiting.setTitle("已连接到服务器");
							break;
						case 3:
							var percent = d.downloadedSize / d.totalSize * 100;
							wgtWaiting.setTitle("已下载 " + parseInt(percent) + "%");
							break;
						case 4:
							wgtWaiting.setTitle("下载完成");
							break;
					}
				});
			}, 5500)
			dtask.start();
		},
		fail: () => {},
		complete: () => {}
	});
}

function updateAppIos(url, version) {
	uni.request({
		url: url + '/real/appVerion', //获取最新版本号
		method: 'POST',
		data: {},
		success: res => {
			if (res.data.data.version != version) {
				console.log("~" + res.data.data.version)
				console.log("~1" + version)
				uni.showToast({
					title: '有新的版本发布...',
					mask: false,
					duration: 5000,
					icon: "none"
				});
				setTimeout(() => {
					uni.showLoading({
						title: '正在打开Safari浏览器...',
					})
				}, 1700)
				setTimeout(() => {
					uni.hideLoading();
					plus.runtime.openURL("http://52sg.vip/ck160", function(res) {
						console.log("shibai")
					})
				}, 2500)
			} else {
				uni.showToast({
					title: '已经是最新版本了',
					mask: false,
					duration: 5000,
					icon: "none"
				});
			}
		},
		fail: () => {},
		complete: () => {}
	});
}
module.exports = {
	updateApp: updateApp,
	plusReady: plusReady,
	updateAppIos: updateAppIos
}
